= Sample Include

== My RSS feed

include::rss[feed=https://feeds.feedburner.com/mrhaki,entries=3]

== JDriven feed

include::rss[feed=https://blog.jdriven.com/atom.xml,entries=10]
