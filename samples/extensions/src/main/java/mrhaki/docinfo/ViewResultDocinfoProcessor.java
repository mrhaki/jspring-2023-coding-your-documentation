package mrhaki.docinfo;

import org.asciidoctor.ast.Document;
import org.asciidoctor.extension.DocinfoProcessor;

public class ViewResultDocinfoProcessor extends DocinfoProcessor {
    private static final String STYLE = """
            <style>
            .listingblock a.view-result {
              float: right;
              font-weight: normal;
              text-decoration: none;
              font-size: 0.9em;
              line-height: 1.4;
              margin-top: 0.15em;
            }
            </style>""";

    private static final String JAVASCRIPT = """
            <script>
            function toggle_result_block(e) {
              var resultNode = e.target.parentNode.parentNode.nextElementSibling;
              resultNode.style.display = getComputedStyle(resultNode).display == 'none' ? '' : 'none';
              return false;
            }
            document.addEventListener('DOMContentLoaded', function() {
              [].forEach.call(document.querySelectorAll('.result'), function(resultNode) {
                resultNode.style.display = 'none';
                var viewLink = document.createElement('a');
                viewLink.className = 'view-result';
                viewLink.appendChild(document.createTextNode('[ view result ]'));
                resultNode.previousElementSibling.querySelector('.title').appendChild(viewLink);
                viewLink.addEventListener('click', toggle_result_block);
              });
            });
            </script>
            """;

    @Override
    public String process(final Document document) {
        return STYLE + System.lineSeparator() + JAVASCRIPT;
    }
}
