package mrhaki.pre;

import org.asciidoctor.ast.Document;
import org.asciidoctor.extension.Preprocessor;
import org.asciidoctor.extension.PreprocessorReader;

import java.util.ArrayList;

/*
    = Sample document
    :draft:
    
    == Chapter 1
    
    // TODO: provide content
     
    == Chapter 2
    
    // FIXME: check spellling    
 */
public class DraftPreprocessor extends Preprocessor {
    @Override
    public void process(final Document document, final PreprocessorReader reader) {
        var draft = document.getAttributes().containsKey("draft");
        
        if (draft) {
            var newLines = new ArrayList<String>();
            
            for (String line : reader.readLines()) {
                if (line.startsWith("// TODO") || line.startsWith("// FIXME")) {
                    newLines.add("****");
                    newLines.add(line.substring(3));
                    newLines.add("****");
                } else {
                    newLines.add(line);
                }
            }
            reader.restoreLines(newLines);
        } 
    }
}
