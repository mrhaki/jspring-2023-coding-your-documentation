package mrhaki.pre;

import org.asciidoctor.ast.Document;
import org.asciidoctor.extension.Preprocessor;
import org.asciidoctor.extension.PreprocessorReader;

import java.util.Map;

/**
 * Preprocessor to add all Java sytem properties that start with
 * <code>java.</code> as document attributes.
 * <p>
 * The <code>.</code> is replaced with <code>-</code>.
 */
public class JavaPropsPreprocessor extends Preprocessor {

    @Override
    public void process(final Document document, final PreprocessorReader reader) {
        System.getProperties()
              .entrySet()
              .stream()
              .filter(entry -> ((String) entry.getKey()).startsWith("java."))
              .forEach(entry -> addDocAttribute(document, entry));
    }

    private void addDocAttribute(Document document, Map.Entry<Object, Object> entry) {
        String attributeName = ((String) entry.getKey()).replace('.', '-');
        // last argument is set to false to indicate we don't
        // want to override an existing document attribute
        // with the same name.
        document.setAttribute(attributeName, entry.getValue(), false);
    }
}
