package mrhaki.pre;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.jruby.extension.spi.ExtensionRegistry;

/**
 * ExtensionRegistry implementation to register {@link JavaPropsPreprocessor}.
 */
public class JavaPropsPreprocessorExtensionRegistry implements ExtensionRegistry {
    @Override
    public void register(final Asciidoctor asciidoctor) {
        asciidoctor.javaExtensionRegistry().preprocessor(JavaPropsPreprocessor.class);
    }
}
