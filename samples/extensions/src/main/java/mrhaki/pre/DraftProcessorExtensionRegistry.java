package mrhaki.pre;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.jruby.extension.spi.ExtensionRegistry;

/**
 * ExtensionRegistry to register {@link DraftPreprocessor}.
 */
public class DraftProcessorExtensionRegistry implements ExtensionRegistry {
    @Override
    public void register(final Asciidoctor asciidoctor) {
        asciidoctor.javaExtensionRegistry().preprocessor(DraftPreprocessor.class);
    }
}
