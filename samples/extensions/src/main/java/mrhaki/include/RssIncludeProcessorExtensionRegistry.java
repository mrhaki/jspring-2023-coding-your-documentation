package mrhaki.include;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.jruby.extension.spi.ExtensionRegistry;

/**
 * ExtensionRegistry implementation to register {@link RssIncludeProcessor}.
 */
public class RssIncludeProcessorExtensionRegistry implements ExtensionRegistry {
    @Override
    public void register(final Asciidoctor asciidoctor) {
        asciidoctor.javaExtensionRegistry().includeProcessor(RssIncludeProcessor.class);
    }
}
