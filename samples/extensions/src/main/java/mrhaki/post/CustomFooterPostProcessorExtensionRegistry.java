package mrhaki.post;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.jruby.extension.spi.ExtensionRegistry;

/**
 * ExtensionRegistry to register {@link CustomFooterPostProcessor}.
 */
public class CustomFooterPostProcessorExtensionRegistry implements ExtensionRegistry {
    @Override
    public void register(final Asciidoctor asciidoctor) {
        asciidoctor.javaExtensionRegistry().postprocessor(CustomFooterPostProcessor.class);
    }
}
