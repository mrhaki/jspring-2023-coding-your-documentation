package mrhaki.app;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Attributes;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.asciidoctor.ast.Document;

import java.io.File;

/**
 * Sample Java application to show several ways on how to work
 * with the {@link Asciidoctor} instance.
 */
public class AsciidoctorApp {

    public static void main(String[] args) {
        convertFile();
        convertText();
        readStructure();
    }

    private static void convertFile() {
        try (Asciidoctor asciidoctor = Asciidoctor.Factory.create()) {
            asciidoctor.convertFile(
                    new File("sample.adoc"),
                    Options.builder()
                           .toFile(true)
                           .backend("html")
                           .safe(SafeMode.UNSAFE)
                           .attributes(Attributes.builder()
                                                 .attribute("source-highlighter", "highlight.js")
                                                 .build())
                           .build()
            );
        }
    }
    
    private static void convertText() {
        try (Asciidoctor asciidoctor = Asciidoctor.Factory.create()) {
            String result = asciidoctor.convert(
                    """
                            == Sample 
                            
                            *Hello {conference}!""",
                    Options.builder()
                           .safe(SafeMode.UNSAFE)
                           .attributes(Attributes.builder()
                                                 .attribute("conference", "JFall 2022")
                                                 .build())
                           .build());

            System.out.println(result);
        }
    }

    private static void readStructure() {
        try (Asciidoctor asciidoctor = Asciidoctor.Factory.create()) {
            Document document = asciidoctor.loadFile(
                    new File("sample.adoc"),
                    Options.builder().safe(SafeMode.UNSAFE).build());

            System.out.println("document.title -> " + document.getTitle());
            System.out.println("# blocks -> " + document.getBlocks().size());
            System.out.println("section[0].title -> " + document.getBlocks().get(0).getTitle());
        }
    }
}
