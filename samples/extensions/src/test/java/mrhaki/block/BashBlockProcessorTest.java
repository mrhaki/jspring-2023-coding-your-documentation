package mrhaki.block;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.junit.jupiter.api.Test;

import java.io.File;

class BashBlockProcessorTest {

    @Test
    void renderBashBlock() {
        try (Asciidoctor asciidoctor = Asciidoctor.Factory.create()) {
            asciidoctor.convertFile(
                    new File("bash.adoc"),
                    Options.builder()
                           .safe(SafeMode.UNSAFE)
                           .build()
            );
        }
    }
}
