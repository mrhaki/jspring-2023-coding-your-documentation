package mrhaki.block;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.junit.jupiter.api.Test;

import java.io.File;

class TootBlockMacroProcessorTest {
    
    @Test
    void createTootBlock() {
        try (Asciidoctor asciidoctor = Asciidoctor.Factory.create()) {
            // when
            asciidoctor.convertFile(
                    new File("toot.adoc"),
                    Options.builder()
                           .toFile(true)
                           .safe(SafeMode.UNSAFE)
                           .build()
            );
        }
    }
}
