plugins {
    `java-library`
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(libs.asciidoctorj)
    implementation(libs.asciidoctorj.pdf)
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(17))
    }
}


